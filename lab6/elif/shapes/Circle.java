package elif.shapes;

public class Circle {
	
	//int radius;
	//we write nothing before 'int' so only classes in the same package can access this fields (package visible)//
	
	public int radius;
	
	//private int radius;
	//only the this class can access.//
	
	public Circle(int radius){
		this.radius = radius;
	}
	
	public double area(){
		return Math.PI * radius * radius;
	}
}
