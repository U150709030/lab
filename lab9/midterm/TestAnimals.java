package midterm;
import java.util.ArrayList;

public class TestAnimals {

	public static void main(String[] args) {
		ArrayList<Animal> animals = new ArrayList<Animal>();
		
		Cat cat = new Cat("Tom");
		animals.add(cat);
		
		Dog dog = new Dog("Scooby Doo");
		animals.add(dog);
		
		Duck duck = new Duck("Scooby Doo");
		animals.add(duck);
		
		for(Animal animal: animals){
			System.out.print(animal.getName() + ", ");
		}
		
			System.out.println();
			
		for(Animal animal: animals){
			System.out.print(animal.speak() + ", ");
		}
	}
}
