package midterm;

public class Question4 {
	//Method to check if given number is prime or not
	public static boolean isPrime(int n){	
		for(int i = 2; i<n; i++){
			if (n % i == 0)
				return false;
		}
		
		return true;
	}
	
	
	public static void main(String[] args) {
		
		int[] primeNumbers = new int[50];
		//Initialized integer number to 2 
		int number = 2;
		//Integer a is going to keep track of the index 
		//and it will control the amount of iterations
		int a = 0;
		
		while(a<50){
			if(isPrime(number)){
				primeNumbers[a] = number;
				number++;
				a++;
			}else{
				while(!isPrime(number))
					number++;
			}
		}

		for(int i = 0; i<50; i++){
			System.out.print(primeNumbers[i] + " ");
		}
	}
}

