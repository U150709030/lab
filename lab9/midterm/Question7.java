package midterm;

public class Question7 {
	
	public static String hexa(int n){
		String[] a = {"0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"};
		if (n <= 0){
			return "";
		}
		return hexa(n / 16) + a[n%16];
	}
	
	public static void main(String[] args) {
		System.out.println(hexa(175));
	}

}

